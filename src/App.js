

import './App.css';

import ContentComponent from './components/Content-pizza/ContentComponent';
import FooterComponent from './components/Footer-pizza/FooterComponent';
import HeaderComponent from './components/Header-pizza/HeaderComponent';


function App() {
  return (
    <div>
      <HeaderComponent />
      <ContentComponent />
      <FooterComponent />
    </div>

  );
}

export default App;
