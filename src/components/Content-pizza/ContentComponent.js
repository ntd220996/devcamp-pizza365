import { Component } from "react";
import Drink from "./Drink Component/Drink";
import BodyForm from "./Form Component/Body-Form";
import TitleForm from "./Form Component/Title-Form";
import BodyIntroduce from "./Introduce Component/Body-Introduce";
import SlideIntroduce from "./Introduce Component/Slide-Introduce";
import TitleIntroduce from "./Introduce Component/Tiltle-Introduce";
import BodySize from "./Size Component/Body-Size";
import TitleSize from "./Size Component/Title-Size";
import BodyType from "./Type Component/Body-Type";
import TitleType from "./Type Component/Title-Type";


class ContentComponent extends Component {
    render() {
        return (
            <>
                {/* Introduce Component */}
                <SlideIntroduce />
                <TitleIntroduce />
                <BodyIntroduce />

                {/* Type Component */}
                <TitleSize />
                <BodySize />

                {/* TypeComponent */}
                <TitleType />
                <BodyType />

                {/* DrinkComponent */}
                <Drink />

                {/* FormComponent  */}
                < TitleForm />
                <BodyForm />


                <br />
            </>
        )
    }
}
export default ContentComponent