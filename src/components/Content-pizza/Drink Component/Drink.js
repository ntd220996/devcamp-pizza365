import { Component } from "react";

class Drink extends Component {
    render() {
        return (
            <>
                {/* <!-- Title --> */}
                <div class="text-center mb-4 mt-4">
                    <h4><span class="title">Chọn đồ uống</span></h4>
                </div>
                {/* <!-- Content --> */}
                <div class="container">
                    <select name="Drinks" id="" class="form-select">
                        <option value=""> Chọn đồ uống </option>
                    </select>
                </div>
            </>

        )
    }
}

export default Drink