import { Component } from "react";

class BodyForm extends Component {
    render () {
        return (
            <div className="container" id="send">
            <div className="form-group">
              <label className="col-form-label">Tên</label>
              <div><input type="text" name="" id="" placeholder="Nhập tên" className="form-control"/></div>
            </div>
            <div className="form-group">
              <label className="col-form-label">Email</label>
              <div><input type="text" name="" id="" placeholder="Nhập Email" className="form-control"/></div>
            </div>
            <div className="form-group">
              <label className="col-form-label">Số điện thoại</label>
              <div><input type="text" name="" id="" placeholder="Nhập Số điện thoại" className="form-control"/></div>
            </div>
            <div className="form-group">
              <label className="col-form-label">Địa chỉ</label>
              <div><input type="text" name="" id="" placeholder="Nhập Địa chỉ" className="form-control"/></div>
            </div>
            <div className="form-group">
              <label className="col-form-label">Mã giảm gái</label>
              <div><input type="text" name="" id="" placeholder="Nhập mã giảm giá" className="form-control"/></div>
            </div>
            <div className="form-group mb-4">
              <label className="col-form-label">Lời nhắn</label>
              <div><input type="text" name="" id="" placeholder="Nhập lời nhắn" className="form-control"/></div>
            </div>
           {/* <!-- Button --> */}
            <button className="form-control btn btn-warning">Gửi</button>
          </div>
        
        )
    }
}

export default BodyForm