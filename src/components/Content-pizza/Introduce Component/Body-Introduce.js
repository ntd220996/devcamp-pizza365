import { Component } from "react";
// Khai báo object để áp dụng vào CSS
const objectStyleBox = {
    box1: {
        backgroundColor: 'rgba(250,250,210,255)',
        borderColor: 'rgba(255,165,0,255)',
        borderStyle: 'solid',

        borderRight: '0ch',
        borderWidth: '1px'
    },
    box2: {
        backgroundColor: 'rgba(254,254,0,255)',
        borderColor: 'rgba(255,165,0,255)',
        borderStyle: 'solid',
        borderWidth: '2px',
        borderRight: '0ch',
        borderTopWidth: '1px',
        borderBottomWidth: '1px',
        borderLeftColor: 'white'
    },
    box3: {
        backgroundColor: 'rgba(255,161,123,255)',
        borderColor: 'rgba(255,165,0,255)',
        borderStyle: 'solid',
        borderTopWidth: '1px',
        borderBottomWidth: '1px',
        borderWidth: '2px',
        borderLeftColor: 'white',
        borderRightColor: 'white'
    },
    box4: {
        backgroundColor: 'rgba(255,165,0,255)',
        borderColor: 'rgba(255,165,0,255)',
        borderStyle: 'solid',
        borderLeft: '0ch',
        borderWidth: '1px'
    }

}

class BodyIntroduce extends Component {

    render() {
        return (
            <div className="container mb-4">
                <div className="row">
                    {/* <!-- ô 1 --> */}
                    <div className=" col-sm-3 p-4" style={objectStyleBox.box1}>
                        <h4>Đa dạng</h4>
                        <p><b className="text-box">Số lượng pizza đa dạng, có đầy đủ các loại pizaa đang hot nhất hiện nay.</b></p>
                    </div>

                    {/* <!-- ô 2 --> */}
                    <div className="col-sm-3 p-4" style={objectStyleBox.box2}>
                        <h4>Chất lượng</h4>
                        <p><b className="text-box">Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</b></p>
                    </div>

                    {/* <!-- ô 3 --> */}
                    <div className="col-sm-3 p-4" style={objectStyleBox.box3}>
                        <h4>Hương vị</h4>
                        <p><b className="text-box">Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ pizaa 365.</b></p>
                    </div>

                    {/* <!-- ô 4 --> */}
                    <div className="col-sm-3 p-4" style={objectStyleBox.box4}>
                        <h4>Dịch vụ</h4>
                        <p><b class="text-box">Nhân viên thân thiện, nhà hàng hiện đại.Dịch vụ giao hàng nhanh chất lượng, tân tiến.</b></p>
                    </div>
                </div>
            </div>
        )
    }
}

export default BodyIntroduce