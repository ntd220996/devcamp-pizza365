import image1 from '../../../assets/images/1.jpg'
import image2 from '../../../assets/images/2.jpg'
import image3 from '../../../assets/images/3.jpg'
import image4 from '../../../assets/images/4.jpg'
import Carousel from 'react-bootstrap/Carousel';
import { Component } from 'react'

class SlideIntroduce extends Component {
    render() {
        return (
            
            <div className="container mt-4">
                <h3 className="logo">PIZZA 365</h3>
                <p className="slogan">Truly Italian !</p>
                <Carousel >
                    <Carousel.Item interval={4000}>
                        <img
                            className="d-block w-100"
                            src={image1}
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item interval={4000}>
                        <img
                            className="d-block w-100"
                            src={image2}
                            alt="Second slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item interval={4000}>
                        <img
                            className="d-block w-100"
                            src={image3}
                            alt="Third slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item interval={4000}>
                        <img
                            className="d-block w-100"
                            src={image4}
                            alt="Fourth slide"
                        />
                    </Carousel.Item>
                </Carousel>
            </div>
        )
    }
}

export default SlideIntroduce