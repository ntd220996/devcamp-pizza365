import { Component } from "react";

const cardRL = {
    backgroundColor: 'rgba(255,165,0,255)',
    fontSize: '25px',
    fontWight: '600'
}
const buttonCardRL = {
    backgroundColor: 'rgba(255,165,0,255)',
    fontSize: '15px',
    fontWeight: '500',
    color: 'black'
}
const cardM =  {
    backgroundColor: 'rgba(254,192,7,255)',
    fontSize: '25px',
    fontWeight: '600'
}
const buttonCardM = {
    backgroundColor: 'rgba(254,192,7,255)',
    fontSize: '15px',
    fontWeight: '500',
    color: 'black'
}
class BodySize extends Component {
    render() {
        return (
            <div className="container" id="combo">
            <div className="row">
              {/* <!-- Card 1 --> */}
              <div className="col-sm-4">
        
                <div className="card text-center">
                  <div className="card-header cardRL" style={cardRL}>
                    S (small)
                  </div>
                  <div className="card-body">
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">Đường kính: <b>20cm</b></li>
                      <li className="list-group-item">Sườn nướng: <b>2</b></li>
                      <li className="list-group-item">Salad: <b>200g</b></li>
                      <li className="list-group-item">Nước ngọt: <b>2</b></li>
                      <li className="list-group-item p-2"><b class="price">150.000</b> <p>VNĐ</p></li>
                    </ul>
                  </div>
                  <div className="card-footer">
                    <div className="col-sm-12"><button className="form-control" style={buttonCardRL} id="btn-size-S">Chọn</button></div>
                  </div>
                </div>  
        
              </div>
        
              {/* <!-- Card 2 --> */}
              <div className="col-sm-4">
                
                <div className="card text-center">
                  <div className="card-header" style={cardM}>
                    M (Medium)
                  </div>
                  <div className="card-body">
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">Đường kính: <b>25cm</b></li>
                      <li className="list-group-item">Sườn nướng: <b>4</b></li>
                      <li className="list-group-item">Salad: <b>300g</b></li>
                      <li className="list-group-item">Nước ngọt: <b>3</b></li>
                      <li className="list-group-item p-2"><b class="price">200.000</b> <p>VNĐ</p></li>
                    </ul>
                  </div>
                  <div className="card-footer ">
                    <div className="col-sm-12"><button className="form-control" style={buttonCardM}>Chọn</button></div>
                  </div>
                </div>
        
              </div>
        
              {/* <!-- Card 3 --> */}
              <div className="col-sm-4">
        
                <div className="card text-center">
                  <div className="card-header cardRL" style={cardRL}>
                    S (small)
                  </div>
                  <div className="card-body">
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">Đường kính: <b>30cm</b></li>
                      <li className="list-group-item">Sườn nướng: <b>8</b></li>
                      <li className="list-group-item">Salad: <b>500g</b></li>
                      <li className="list-group-item">Nước ngọt: <b>4</b></li>
                      <li className="list-group-item p-2"><b className="price">250.000</b> <p>VNĐ</p></li>
                    </ul>
                  </div>
                  <div className="card-footer ">
                    <div className="col-sm-12"><button className="form-control " style={buttonCardRL}>Chọn</button></div>
                  </div>
                </div> 
              </div>
            </div>
          </div>
        )
    }
}

export default BodySize