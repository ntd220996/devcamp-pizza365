import { Component } from "react";

class TitleSize extends Component {
    render() {
        return (
            <div className="text-center mb-4 mt-5">
                <h4><span className="title">Chọn Size pizza</span></h4>
                <p className="title-2">Chọn combo pizza phù hợp với nhu cầu của bạn</p>
            </div>
        )
    }
}

export default TitleSize