import { Component } from "react";
import seafood from '../../../assets/images/seafood.jpg'
import hawaii from '../../../assets/images/hawaiian.jpg'
import bacon from '../../../assets/images/bacon.jpg'

const buttonCardRL = {
    backgroundColor: 'rgba(255,165,0,255)',
    fontSize: '15px',
    fontWeight: '500',
    color: 'black'
}

const buttonCardM = {
    backgroundColor: 'rgba(254,192,7,255)',
    fontSize: '15px',
    fontWeight: '500',
    color: 'black'
}

class BodyType extends Component {
    render() {
        return (
            <div class="container" id="pizza">
                <div class="row">

                    {/* <!-- Card 1 --> */}
                    <div class="col-sm-4">

                        <div class="card" style={{height: '100%'}}>
                            <img class="card-img-top" src={seafood} alt="Card image cap" />
                                <div class="card-body">
                                    <h5 class="card-title">OCEAN MANIA</h5>
                                    <p class="card-text">PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                    <p class="card-text">Xốt Cà Chua, Phố Mai Mozzarella, Tôm, Mực, Thanh Cua, Hành Tây.</p>
                                </div>
                                <div class="card-footer ">
                                    <div class="col-sm-12"><button class="form-control" style={buttonCardRL}>Chọn</button></div>
                                </div>
                        </div>

                    </div>

                    {/* <!-- Card 2 --> */}
                    <div class="col-sm-4" >

                        <div class="card" style={{height: '100%'}}>
                            <img class="card-img-top" src={hawaii} alt="Card image cap"/>
                                <div class="card-body">
                                    <h5 class="card-title">HAWAIIAN</h5>
                                    <p class="card-text">PIZZA DĂM BỐNG DỨA KIỂU HAWAII</p>
                                    <p class="card-text">Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.</p>
                                </div>
                                <div class="card-footer ">
                                    <div class="col-sm-12"><button class="form-control " style={buttonCardM}>Chọn</button></div>
                                </div>
                        </div>

                    </div>

                    {/* <!-- Card 3 --> */}
                    <div class="col-sm-4">

                        <div class="card" style={{height: '100%'}}>
                            <img class="card-img-top" src={bacon} alt="Card image cap"/>
                                <div class="card-body">
                                    <h5 class="card-title">CHEESY CHICKEN BACON</h5>
                                    <p class="card-text">PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                    <p class="card-text">Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarelaa, Cà Chua</p>
                                </div>
                                <div class="card-footer ">
                                    <div class="col-sm-12"><button class="form-control " style={buttonCardRL}>Chọn</button></div>
                                </div>
                        </div>

                    </div>
                </div>
            </div>

        )
    }
}
export default BodyType