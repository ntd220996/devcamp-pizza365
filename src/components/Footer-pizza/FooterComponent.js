import { Component } from "react";

class FooterComponent extends Component {
    render() {
        return (
            <>
                < div class="container-fluid bg-warning p-4" >
                    <div class="col-sm-12 text-center">
                        <h3 class="mt-2">Footer</h3>
                        <a href="#" class="btn btn-dark"> <i class="fas fa-arrow-up" style={{ color: 'aqua' }}></i> To the top </a>
                        <div class="col-sm-12">
                            <i class="fab fa-facebook icon "></i>
                            <i class="fab fa-instagram icon p-1"></i>
                            <i class="fab fa-google-plus-g icon"></i>
                            <i class="fab fa-google-plus-square icon p-1"></i>
                            <i class="fab fa-twitter icon"></i>
                        </div>
                        <p class="mt-1 text-p"> Powered by DEVCAMP </p>
                    </div>
                </div >
            </>
        )
    }
}

export default FooterComponent