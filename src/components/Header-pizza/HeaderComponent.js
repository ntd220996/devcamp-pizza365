import { Component } from "react";

class HeaderComponent extends Component {
    render() {
        return (
            <nav class="navbar navbar-expand-lg navbar-light div-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button> 
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav nav-fill w-100">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Trang chủ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#combo">Combo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#pizza">Loại pizza</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#send">Gửi đơn hàng</a>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}

export default HeaderComponent